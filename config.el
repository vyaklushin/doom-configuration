;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Vasilii Iakliushin"
      user-mail-address "viakliushin@gitlab.com")


;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one)
(load-theme 'atom-one-dark t)

(setenv "TERM" "xterm-color")

(setq kill-whole-line t)

(custom-set-variables
 '(initial-frame-alist (quote ((fullscreen . maximized)))))
;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Dropbox/org/mode/"
      org-catch-invisible-edits 'smart
      org-agenda-skip-scheduled-if-done t
      org-agenda-files
      (list (expand-file-name "Notes.org" org-directory)
            (expand-file-name "Work.org" org-directory)
            (expand-file-name "Inbox.org" org-directory)))

(after! org
  ;; disable auto-complete in org-mode buffers
  (remove-hook 'org-mode-hook #'auto-fill-mode)
  (add-hook 'auto-save-hook 'org-save-all-org-buffers)

  (setq org-capture-templates
        '(
          ("w" "work" entry (file "Work.org") "* NEXT %?")
          ("i" "inbox" entry (file "Inbox.org") "* NEXT %?")))

  (setq org-todo-keywords
        '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)")))
  (setq org-agenda-custom-commands
        '(("a" "Today"
           ((agenda "" ((org-deadline-warning-days 7)
                        (org-agenda-span 1)
                        (org-agenda-start-day nil)))
            (todo "NEXT"
                 ((org-agenda-overriding-header "Next tasks")))

            ))
          ("w" "Week"
            ((agenda "" ((org-deadline-warning-days 7)
                         (org-agenda-span 7)
                         (org-agenda-start-day nil)
                         (org-agenda-time-grid nil)
                         (org-agenda-start-on-weekday nil)))
            (todo "NEXT"
                 ((org-agenda-overriding-header "Next tasks")))

            ))))

  (setq org-agenda-start-on-weekday 1)
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)
  (setq company-global-modes '(not org-mode)))


(defun org-file-path (filename)
  "Return the absolute address of an org file, given its relative name."
  (concat (file-name-as-directory org-directory) filename))

(map! :leader
      :desc "Switch to Notes.org" "C-n"  #'(lambda () (interactive) (find-file (org-file-path "Notes.org")))
      :desc "Swtich to Work.org" "C-w" #'(lambda () (interactive) (find-file (org-file-path "Work.org")))
      :desc "Swtich to Inbox.org" "C-m" #'(lambda () (interactive) (find-file (org-file-path "Inbox.org"))))

(after! go-mode
  (setq gofmt-command "goimports")
  (add-hook 'before-save-hook 'gofmt-before-save))

(map!
 "M-`" #'+popup/toggle)

(after! magit
  (set-popup-rule! "^\\(?:\\*magit\\|magit:\\| \\*transient\\*\\)" :size 35 :select t :ttl nil :quit nil)
  (remove-hook 'magit-status-sections-hook 'magit-insert-tags-header)
  (remove-hook 'magit-status-sections-hook 'magit-insert-unpushed-to-pushremote)
  (remove-hook 'magit-status-sections-hook 'magit-insert-unpulled-from-pushremote)
  (remove-hook 'magit-status-sections-hook 'magit-insert-unpulled-from-upstream))

(after! git-commit
  (setq git-commit-summary-max-length 70))


(after! neotree
  (setq neo-smart-open t))


(after! projectile
    (defun smart-projectile-action ()
      (funcall 'projectile-dired)
      (funcall 'treemacs-add-and-display-current-project))

  (setq projectile-switch-project-action #'smart-projectile-action))


(map!
 "C-c C-g" #'avy-goto-line
 )
(map!
 "C-c g" #'magit-status-quick
 "C-c b" #'magit-blame
 )

(after! git-link
  (setq git-link-use-commit t)
  (setq git-link-open-in-browser t)
  )

(map!
 "C-c l" #'git-link
 )

(map!
 "M-RET" #'+fold/toggle
 )

(map!
 "s-<up>" #'move-text-up
 "s-<down>" #'move-text-down
 )

(map!
 "C-c i" #'bundle-install)



(map!
 "C-|" #'treemacs-find-file
 )


(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))

(after! treemacs
  (setq treemacs-recenter-after-file-follow "always"))


(map! "C-s" #'+default/search-buffer)

(map!
 "M-m" #'avy-goto-char
 )
(map!
 "C-x C-z" #'zoom-window-zoom

 )

(dimmer-mode t)

;; Custom settings
(windmove-default-keybindings 'meta)
(setq mac-command-modifier 'meta)
(setq mac-option-modifier 'super)

(set-frame-font "Monaco 12" nil t)

(defun duplicate-current-line-or-region (arg)
  "Duplicates the current line or region ARG times."
  (interactive "p")
  (let (beg end (origin (point)))
    (if (and mark-active (> (point) (mark)))
        (exchange-point-and-mark))
    (setq beg (line-beginning-position))
    (if mark-active
        (exchange-point-and-mark))
    (setq end (line-end-position))
    (let ((region (buffer-substring-no-properties beg end)))
      (dotimes (i arg)
        (goto-char end)
        (newline)
        (insert region)
        (setq end (point)))
      (goto-char (+ origin (* (length region) arg) arg)))))

(map!
 "C-c C-d" #'duplicate-current-line-or-region
 )

(after! fill-column-indicator
  (setq fci-rule-column 80)
  (add-hook 'prog-mode-hook 'fci-mode)
  )

(after! ivy
  (setq ivy-count-format "(%d/%d) ")
  (setq ivy-use-virtual-buffers t))

(defadvice switch-to-buffer (before save-buffer-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice other-window (before other-window-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice windmove-up (before other-window-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice windmove-down (before other-window-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice windmove-left (before other-window-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice windmove-right (before other-window-now activate)
  (when buffer-file-name (save-buffer)))


(setq enable-recursive-minibuffers nil)


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
